FROM ubuntu:latest
RUN apt-get update -y \
    && apt-get install -y software-properties-common \
    && apt-add-repository universe \
    && apt-get update \
    && apt-get install -y python3.8 \
    && apt-get install -y python3-pip python-dev build-essential
COPY ./app /app
WORKDIR /app
RUN pip3 install -r requirements.txt \
    && apt-get install -y libpango1.0-0 \ 
    && apt-get install -y libcairo2 
ENTRYPOINT ["python3","main.py"]
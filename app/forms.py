from wtforms import validators, TextField, Form

class ReusableForm(Form):
    name = TextField('Name:', validators=[validators.required()])
    course = TextField('Course:', validators=[validators.required()])

class LoginForm(Form):
    username = TextField('Username:', validators=[validators.required()])
    password = TextField('Password:', validators=[validators.required()])
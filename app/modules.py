import time
import datetime
import ast
import random

month = {
    'June': 'Червень',
    'July': 'Липень',
    'August': 'Серпень'
}

def get_time():
#    time = strftime("%Y-%m-%dT%H:%M")
    year = time.strftime("%Y")
    now = datetime.datetime.now()
    mounth_en = now.strftime('%B')
    mounth = month.get(mounth_en)
    date = '{} {}'.format(mounth, year)
    return date

def write_to_disk(name, course, manager):
    data = open('file.log', 'a')
    timestamp = get_time()
    data.write('DateStamp={}, name={}, course={}, manager={} \n'.format(timestamp, name, course, manager))
    data.close()

def id_generator():
    id_list = []
    for i in range(5):
        ran = random.randint(0,9)
        id_list.append(ran)
    magic = lambda id_list: int(''.join(str(i) for i in id_list))
    result = magic(id_list)
    #print(result)
    return result

def send_data(name, course, manager):
    data = {'data':[]}
    # file_to_write = open('file.log', 'a')
    timestamp = get_time()
    # f = open("file.log", "r")
    # file_to_read = f.read()
    # data = dict(ast.literal_eval(file_to_read))
    student_id = id_generator()
    data['data'].append({'student_name': name, 'course_name':course, 'project_manager':manager, 'current_date': timestamp, 'student_id':'CD {}'.format(student_id)})
    # open('file.log', 'w').close()
    # file_to_write.write(str(data))
    # file_to_write.close()
    return data

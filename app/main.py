from random import randint
from flask import Flask, render_template, flash, request, jsonify, send_file, redirect
from forms import ReusableForm, LoginForm
from modules import get_time, write_to_disk, send_data
from text_replacer import TextReplacer 

DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = 'codeclub'
right_login = 'CodeclubUser'
right_pass = 'codeclubpass'

@app.route("/", methods=['GET', 'POST'])
def hello():
    form = ReusableForm(request.form)

    #print(form.errors)
    if request.method == 'POST':
        name=request.form['name']
        course=request.form['email']
        manager=request.form['surname']
        status= request.form['StatusRadios']
        label= request.form.get('It2SchoolBox', False)
        if name:
            stream_data = send_data(name, course, manager)
            flash('Certificate of {} is ready!'.format(name))
            path = ''
            if status == 'Student':
                if label == 'true':
                    path += 'it2schoolcert.svg'
                else:
                    path += 'certificate.svg'
            else:
                if label == 'true':
                    path += 'it2schoolgreet.svg'
                else:
                    path += 'greetings.svg'

            student_certificate = TextReplacer(path, stream_data)
            return send_file(filename_or_fp='{}'.format(student_certificate.image_render()), as_attachment=True)
        else:
            flash('Error: All Fields are Required')

    return render_template('index.html', form=form)

@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        print(username, password)
        if username==right_login and password == right_pass:
            return redirect(url_for('/'))
    return render_template('login.html', form = form) 
            

if __name__ == "__main__":
    app.run(host='0.0.0.0')

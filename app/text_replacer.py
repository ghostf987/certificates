import os, re
import cairosvg

class TextReplacer:
    def __init__(self, template_path: str, info_data: dict):
        self.template_path = template_path
        self.insertion_data = info_data
        self.template_data = open('certificates/' + template_path, 'r').read()
        self.certificate_names = []

        assert template_path.endswith('.svg'), 'Необходим формат SVG'

    def marker_replace(self, insertion_data):
        insertion_data = dict((re.escape(f'${k}$'), v) for k, v in insertion_data.items())
        pattern = re.compile("|".join(insertion_data.keys()))
        text = pattern.sub(lambda m: insertion_data[re.escape(m.group(0))], self.template_data)

        return text

    def certificates_saver(self, insertion_data):
        formatted_file = self.marker_replace(insertion_data)
        file_name = f'assets/{insertion_data["student_name"].replace(" ", "_").lower()}.svg'
        new_file = open(file_name, 'w+')
        new_file.write(formatted_file)
        new_file.close()

        return file_name

    def assets_folder_create(self):
        path = 'assets'
        try:
            os.mkdir(path)
            return True
        except FileExistsError:
            return True
        except OSError:
            print("Ошибка: Невозможно создать папку")
            return False


    def packing(self):
        if self.assets_folder_create():
            for student_info in self.insertion_data['data']:
                    file_name = self.certificates_saver(student_info)
                    self.certificate_names.append(file_name)

    
    def image_render(self):
        self.packing()
        for file_name in self.certificate_names:
            name = f'{file_name.replace(".svg", ".png")}'
            cairosvg.svg2png(url= file_name, write_to=name)

        return name


# r = TextReplacer("certificate.svg", {'data': [{'student_name': 'Dima', 'course_name': 'Python', 'project_manager': 'Gay', 'current_data': 'June 2020', 'student_id': 'CD 5000000'},]})
# r.image_render()